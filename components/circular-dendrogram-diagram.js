function showCircularDendrogram(){
    // Set the dimensions and margins of the graph
    const width = 600;
    const height = 600;

    // Load the CSV file
    d3.csv("final_clean_data.csv").then(data => {
      // Prepare the data for dendrogram
      const hierarchicalData = d3.stratify()
      .id(d => d.District)
      .parentId(d => d.Bicarbonates === "" ? null : d.Bicarbonates)
      (data);

      const root = d3.hierarchy(hierarchicalData);

      // Set the desired circle radius range
      const radius = d3.scaleLinear()
        .domain(d3.extent(root.descendants(), d => d.data.Value))
        .range([20, 150]);

      // Create the cluster layout
      const cluster = d3.cluster()
        .size([360, (width / 2) - 120]);

      // Assign colors to nodes
      const color = d3.scaleOrdinal(d3.schemeCategory10);

      // Create the SVG
      const svg = d3.select("#dendrogram-chart")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", `translate(${width / 2},${height / 2})`);

      // Generate the circular dendrogram layout
      cluster(root);

      // Draw the links
      svg.selectAll(".link")
        .data(root.links())
        .enter()
        .append("path")
        .attr("class", "link")
        .attr("d", d3.linkRadial()
          .angle(d => d.x)
          .radius(d => radius(d.y))
        );

      // Draw the nodes
      svg.selectAll(".node")
        .data(root.descendants())
        .enter()
        .append("circle")
        .attr("class", "node")
        .attr("r", d => radius(d.data.Value))
        .attr("cx", d => d3.pointRadial(d.x, d.y)[0])
        .attr("cy", d => d3.pointRadial(d.x, d.y)[1])
        .attr("fill", d => color(d.depth));

      // Add labels to the nodes
      svg.selectAll(".label")
        .data(root.descendants())
        .enter()
        .append("text")
        .attr("class", "label")
        .attr("dy", "0.31em")
        .attr("transform", d => `rotate(${d.x - 90}) translate(${radius(d.y) + 6})${d.x < 180 ? "" : " rotate(180)"}`)
        .attr("text-anchor", d => d.x < 180 ? "start" : "end")
        .text(d => d.data.District)
        .style("font-size", "12px")
        .style("fill", "black");
    });
}

showCircularDendrogram();

// Load the CSV file
// async function loadData() {
//   try {
//     const data = await d3.csv("final_clean_data.csv");

//     // Prepare the data for dendrogram
//     const hierarchicalData = d3.stratify()
//       .id(d => d.District)
//       .parentId(d => d.Bicarbonates === "" ? null : d.Bicarbonates)
//       (data);

//   } catch (error) {
//     console.log("Error loading data:", error);
//   }
// }

// // Call the async function to load the data and create the Circular Dendrogram
// loadData();
