function showAlluvialDiagram(){
    // Set the dimensions and margins of the diagram
    const width = 1000;
    const height = 1500;
    const margin = { top: 20, right: 10, bottom: 20, left: 10 };

    // Load the CSV file
    d3.csv("final_clean_data.csv").then(data => {
      // Prepare the data
      const districtData = Array.from(new Set(data.map(d => d.district)));
      const villageData = Array.from(new Set(data.map(d => d.village)));

      const nodes = districtData.concat(villageData);
      const links = data.map(d => ({
        source: districtData.indexOf(d.district),
        target: districtData.length + villageData.indexOf(d.village),
        value: 1
      }));


      // Create the color scale based on village count
      const villageCount = d3.rollup(data, v => v.length, d => d.district);
 
      // Create the Sankey layout
      const sankey = d3.sankey()
    //   .nodeId((d) => d.id)
        .nodeWidth(15)
        .nodePadding(10)
        .extent([[margin.left, margin.top], [width - margin.right, height - margin.bottom]])
        .iterations(0);

      const { nodes: sankeyNodes, links: sankeyLinks } = sankey({
        nodes: nodes.map(name => ({ name })),
        links: links
      });

      // compute the sankey network (calculate size, define x and y positions.)

      // Create the SVG
      const svg = d3.select("#alluvial-chart")
        .attr("width", width)
        .attr("height", height);
// Create the color scale for stroke colors
// const colorScale = d3.scaleOrdinal()
//   .domain([...new Set(sankeyNodes.map(d => d.value))])
//   .range(["red", "purple", "orange", "blue", "rgb(111,78,55)", "pink", "purple"]);
  const colorScale = d3.scaleOrdinal()
        .domain(sankeyNodes)
        .range(d3.schemeCategory10);

      // Draw the links
      svg.append("g")
        .selectAll("path")
        .data(sankeyLinks)
        .join("path")
        .attr("d", d3.sankeyLinkHorizontal())
        .attr("fill", "none")
        .attr("stroke", d => colorScale(d.source.value))
        .attr("stroke-opacity", 0.9)
        .attr("stroke-width", d => Math.max(8, d.width))
        .append("title") 
        .text("d => d.source.value");


// Set the minimum height for nodes
const minimumNodeHeight = 3;

// Draw the nodes
svg.append("g")
  .selectAll("rect")
  .data(sankeyNodes)
  .join("rect")
  .attr("x", d => d.x0)
  .attr("y", d => d.y0)
  .attr("height", d => Math.max(minimumNodeHeight, d.y1 - d.y0)) // Use Math.max to ensure a minimum height
  .attr("width", d => d.x1 - d.x0)
  .attr("fill", "#000")
  .attr("stroke", "#000");



      // Draw the node labels
      svg.append("g")
        .selectAll("text")
        .data(sankeyNodes)
        .join("text")
        .attr("x", d => d.x0 < width / 2 ? d.x1 + 6 : d.x0 - 6)
        .attr("y", d => (d.y1 + d.y0) / 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", d => d.x0 < width / 2 ? "start" : "end")
        .text(d => d.name)
        .attr("fill", "#000")
        .attr("font-size", 12)
        .attr("font-family", "sans-serif");


// // Draw the node labels for source nodes (districts)
// svg.append("g")
//   .selectAll(".district-label")
//   .data(sankeyNodes.filter(d => d.sourceLinks.length)) // Filter nodes representing source nodes (districts)
//   .join("text")
//   .attr("class", "district-label")
//   .attr("x", d => d.x0 + 6) // Adjust the label position as needed
//   .attr("y", d => (d.y1 + d.y0) / 2)
//   .attr("dy", "0.35em")
//   .attr("text-anchor", "start")
//   .text(d => d.name)
//   .attr("fill", "#000")
//   .attr("font-size", 12)
//   .attr("font-family", "sans-serif");

    });
}

showAlluvialDiagram();