function showMultiSetBar(){
    // Set the dimensions and margins of the graph
    const margin = { top: 20, right: 20, bottom: 60, left: 60 };
    const width = 1200 - margin.left - margin.right;
    const height = 400 - margin.top - margin.bottom;

    // Load the CSV file
    d3.csv("final_clean_data.csv", d3.autoType).then(data => {


        
      // Extract the categories and values
      const categories = data.map(d => d.District);

      const keys = data.columns.slice(17); // Get the column names excluding the first column (Category)

      // Create the color scale for the categories
      const colorScale = d3.scaleOrdinal()
        .domain(keys)
        .range(d3.schemeCategory10);

      // Create the stacked data
      const stackedData = d3.stack().keys(keys)(data);

      // Set up the scales
      const xScale = d3.scaleBand()
        .domain(categories)
        .range([margin.left, width + margin.left])
        .padding(0.2);

      const yScale = d3.scaleLinear()
        .domain([0, d3.max(stackedData, d => d3.max(d, d => d[1]))])
        .range([height + margin.top, margin.top]);

      // Create the SVG
      const svg = d3.select("#multi-set-chart")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom);

      // Create the groups for the stacked bars
      const groups = svg.selectAll(".bar-group")
        .data(stackedData)
        .join("g")
        .attr("class", "bar-group")
        .style("fill", d => colorScale(d.key));

      // Draw the stacked bars
    //   groups.selectAll("rect")
    //   .data(d => d)
    //   .join("rect")
    //   .attr("x", d => xScale(d.data.District))
    //   .attr("y", d => (d[1] < d[0]) ? yScale(d[0]) : yScale(d[1]))
    //   .attr("height", d => Math.abs(yScale(d[0]) - yScale(d[1])))
    //   .attr("width", xScale.bandwidth());
    
// Draw the separate vertical bars
groups.selectAll(".bar")
  .data(d => d)
  .join("rect")
  .attr("class", "bar")
  .attr("x", (d, i) => xScale(d.data.District) + xScale.bandwidth() / keys.length * i)
  .attr("y", d => yScale(d[1]))
  .attr("height", d => Math.abs(yScale(d[0]) - yScale(d[1])))
  .attr("width", xScale.bandwidth() / keys.length)
//   .attr("fill", d => colorScale(d.key));




      // Draw the horizontal axis
      const xAxis = d3.axisBottom(xScale);
      svg.append("g")
        .attr("transform", `translate(0, ${height + margin.top})`)
        .call(xAxis)
        .selectAll("text")
        .attr("transform", "rotate(-45)")
        .style("text-anchor", "end");

      // Draw the vertical axis
      const yAxis = d3.axisLeft(yScale);
      svg.append("g")
        .attr("transform", `translate(${margin.left}, 0)`)
        .call(yAxis);

      // Draw the legend
      const legend = svg.append("g")
        .attr("transform", `translate(${width - margin.right}, ${margin.top})`);

      legend.selectAll("rect")
        .data(keys)
        .join("rect")
        .attr("x", 0)
        .attr("y", (d, i) => i * 20)
        .attr("width", 10)
        .attr("height", 10)
        .attr("fill", d => colorScale(d));

      legend.selectAll("text")
        .data(keys)
        .join("text")
        .attr("x", 15)
        .attr("y", (d, i) => i * 20 + 9)
        .text(d => d);
    });
}

showMultiSetBar();