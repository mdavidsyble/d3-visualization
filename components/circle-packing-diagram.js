function circlePackingDiagram(){
    // Set the dimensions and margins of the graph
    const width = 600;
    const height = 600;

    // Load the CSV file
    d3.csv("final_clean_data.csv").then(data => {
      // Extract data for the circle packing
const root = {
    name: "root",
    children: data.map(d => ({
      name: d.District,
      children: Object.entries(d)
        .slice(1, 14)
        .map(([key, value]) => ({ name: key, value: +value }))
    }))
  };

      // Create the pack layout
      const pack = d3.pack()
        .size([width, height])
        .padding(10);

      // Assign colors to categories
      const color = d3.scaleOrdinal(d3.schemeCategory10);

      // Create the SVG
      const svg = d3.select("#circle-packing-chart")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", `-${width / 2} -${height / 2} ${width} ${height}`)
        .style("display", "block")
        .style("margin", "0 auto");

      // Generate the circle packing layout
      const hierarchyData = d3.hierarchy(root)
        .sum(d => d.value);
        
      const packedData = pack(hierarchyData);

      // Create the circle packing diagram
      const nodes = svg.selectAll("circle")
        .data(packedData.descendants())
        .join("circle")
        .attr("cx", d => d.x)
        .attr("cy", d => d.y)
        .attr("r", d => d.r)
        .attr("fill", d => color(d.depth));

      // Add labels to the circles
      const labels = svg.selectAll("text")
        .data(packedData.descendants())
        .join("text")
        .attr("text-anchor", "middle")
        .attr("x", d => d.x)
        .attr("y", d => d.y)
        .text(d => d.data.name)
        .style("font-size", "12px")
        .style("fill", "white");
    });
}