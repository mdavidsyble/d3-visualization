function showPieChart(){
        // Set the dimensions and margins of the graph
        const width = 400;
        const height = 400;
        const radius = Math.min(width, height) / 2;
    
        // Load the CSV file
        d3.csv("final_clean_data.csv").then(data => {
        // Extract numerical values and labels for the pie chart
      const values = Object.values(data[0]).slice(17).map(d => +d);
      const labels = Object.keys(data[0]).slice(17);

      // Set up the pie generator
      const pie = d3.pie()
        .value(d => d);

      // Set up the arc generator
      const arc = d3.arc()
        .innerRadius(0)
        .outerRadius(radius);

      // Create the SVG for the pie chart
      const svg = d3.select("#pie-chart")
        .attr("width", width)
        .attr("height", height);

      // Create a group for the pie chart
      const pieGroup = svg.append("g")
        .attr("transform", `translate(${width / 2}, ${height / 2})`);

      // Generate the pie slices
      const slices = pieGroup.selectAll("path")
        .data(pie(values))
        .join("path")
        .attr("d", arc)
        .attr("fill", (d, i) => d3.schemeCategory10[i % d3.schemeCategory10.length]);

      // Add labels to the pie slices
      slices.append("text")
        .attr("transform", d => `translate(${arc.centroid(d)})`)
        .attr("dy", "0.35em")
        .text((d, i) => labels[i])
        .style("text-anchor", "middle");

      // Create the SVG for the legend
      const legendSvg = d3.select("#legend")
        .attr("width", width)
        .attr("height", height);

      // Create a group for the legend items
      const legendGroup = legendSvg.append("g")
        .attr("transform", `translate(20, 20)`);

      // Create the color legend
      const legendItems = legendGroup.selectAll(".legend-item")
        .data(labels)
        .join("g")
        .attr("class", "legend-item")
        .attr("transform", (d, i) => `translate(0, ${i * 20})`);

      // Add color rectangles to the legend
      legendItems.append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", 10)
        .attr("height", 10)
        .attr("fill", (d, i) => d3.schemeCategory10[i % d3.schemeCategory10.length]);

      // Add labels to the legend
      legendItems.append("text")
        .attr("x", 20)
        .attr("y", 10)
        .text(d => d);
    });
}

showPieChart();